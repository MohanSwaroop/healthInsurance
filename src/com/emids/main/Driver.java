package com.emids.main;

import java.text.NumberFormat;

public class Driver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

				String name=" Norman Gomes";
				String gender="male";
				int age=34;
				//health problem
				String hyperTension="false";
				String bloodPressure="false";
				String bloodSugar="false";
				String overWeight="true";
				//Bad and good habits
				String smoking="false";
				String alcohol="true";
				String dailyExercise="true";
				String drugs="false";
				
				float basePremium = 5000;
				String mr = "";

				if (age >= 18) {
					basePremium *= 1.1;
				}
				if (age >= 25) {
					basePremium *= 1.1;
				}
				if (age >= 30) {
					basePremium *= 1.1;
				}
				if (age >= 35) {
					basePremium *= 1.1;
				}
				if (age >= 40) {
					// Add 20% per 5 years above 40
					 age = age - 40;
					while (age >= 5) {
						basePremium *= 1.2;
						age -= 5;
					}
				}
				
				if (gender.equals("male")) {
					mr = "Mr.";
					basePremium *= 1.02;
				}	
				
				if (hyperTension.equals("true")) {
					basePremium *= 1.01;
				}
				if (bloodSugar.equals("true")) {
					basePremium *= 1.01;
				}
				if (bloodPressure.equals("true")) {
					basePremium *= 1.01;
				}
				if (overWeight.equals("true")) {
					basePremium *= 1.01;
				}
				//Good Habit so reduced 3%
				if (dailyExercise.equals("true")) {
					basePremium *= 1.03;
				}
				if (smoking.equals("true")) {
					basePremium *= 0.97;
				}
				if (alcohol.equals("true")) {
					basePremium *= 0.97;
				}
				if (drugs.equals("true")) {
					basePremium *= 0.97;
				}
				NumberFormat nf = NumberFormat.getInstance();
				
				System.out.println("Health Insurance Premium for "+mr+""+name+": Rs."+nf.format(basePremium));
				
	}
}



